﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenemanage : MonoBehaviour
{
    [SerializeField] private string SceneName;
    public void NextScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void Exit()
    {
        Debug.Log("Exit!!!");
        Application.Quit();
        DestroyRemainingShips();
    }
    private void DestroyRemainingShips()
    {
        var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in remainingEnemy)
        {
            Destroy(enemy);
        }
        var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in remainingPlayer)
        {
            Destroy(player);
        }
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneName);

    }

}
